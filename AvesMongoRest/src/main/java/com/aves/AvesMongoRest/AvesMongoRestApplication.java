package com.aves.AvesMongoRest;

import org.springframework.boot.SpringApplication;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.cors.reactive.UrlBasedCorsConfigurationSource;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvesMongoRestApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvesMongoRestApplication.class, args);
	}
	
}
