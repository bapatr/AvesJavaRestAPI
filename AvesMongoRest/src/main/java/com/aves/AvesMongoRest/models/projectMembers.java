package com.aves.AvesMongoRest.models;

import java.util.List; 
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "project_members")
public class projectMembers {
    @Id
    String _id;
    String name;
    String email;
    String bio;
    String src_id;
    String photo;
    List<skill> skills ; 

    public projectMembers() {
    }

    public projectMembers(String name, String email, String bio, List<skill> skills, String photo, String src_id) {
        this.name = name;
        this.email = email;
        this.bio = bio;
        this.skills = skills ; 
        this.photo = photo;
        this.src_id = src_id ;         
    }
    
    public List<skill> getSkills() {
        return skills;
    }

    public void setSkills(List<skill> skills) {
        this.skills = skills;
    }


    public String get_Id() {
        return _id;
    }

    public void set_Id(String _id) {
        this._id = _id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    public String getSrc_id() {
        return src_id;
    }

    public void setSrc_id(String src_id) {
        this.src_id = src_id;
    }    
    
    
    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }    
    

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }
}
