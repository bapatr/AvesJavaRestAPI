package com.aves.AvesMongoRest.repositories;

import com.aves.AvesMongoRest.models.projectMembers;
import org.springframework.data.repository.CrudRepository;

public interface ProjectMemberRepository extends CrudRepository<projectMembers, String> {
    @Override
    java.util.Optional<projectMembers> findById(String id);

    @Override
    void delete(projectMembers deleted);
}
